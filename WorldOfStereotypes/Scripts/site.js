﻿jQuery('#vmap').vectorMap(
    {
        map: 'world_en',
        backgroundColor: '#fff',
        borderColor: 'black',
        borderOpacity: 0.25,
        borderWidth: 0.5,
        color: '#D6E88E',
        regionStyle: {
            initial: {
                fill: '#d6e88e'
            }
        },
        
        enableZoom: true,
        hoverColor: '#D6E88E',
        hoverOpacity: 0.8,
        normalizeFunction: 'linear',
        scaleColors: ['#b6d6ff', '#005ace'],
        selectedColor: '#c9dfaf',
        selectedRegions: null,
        showTooltip: true,
        onRegionClick: function (element, code, region) {
            $.ajax({
                url: "/Home/GetCountryInfo/",
                data: { code: code },
                success: function (view) {
                    $('.singleCountrySection').html(view);
                    $('.loginSection').hide();
                    $('.singleCountrySection').fadeIn(300);
                },

            });
            
            //var message = 'You clicked "'
            //    + region
            //    + '" which has the code: '
            //    + code.toUpperCase();

            //alert(message);
        }
    });

function closeSingleCountrySection() {

    $('.singleCountrySection').fadeOut(300);
};

function showLoginContainer() {
    closeModals();
    $('.loginSection').fadeIn(300);
};

function showRegisterSection() {
    closeModals();
    $('.registerSection').fadeIn(300);
};

function closeLoginSection() {
    
    $('.loginSection').hide();
};

function logUser(login, password) {
    
    $.ajax({
        url: "/Home/Login/",
        dataType: 'json',
        data: { log: login, pas: password },
        success: function (response) {
            if (response.success) {
                window.location.reload();
            } else {
                alert("Invalid login or/and password!");
                
                
                
            }
           
        }, error: function () {
            
            window.location.reload();
            
        }

    });
};

function regUser(name, email, login, password) {
    $.ajax({
        url: "/Home/Register/",
        dataType: 'json',
        data: { nam: name, email: email, log: login, pas: password },
        success: function (response) {
            if (response.success) {
                window.location.reload();
            } else {
                alert(response.responseText);



            }

        }, error: function () {

            window.location.reload();

        }

    });
}

function closeModals() {
    $('.singleCountrySection').hide();
    $('.loginSection').hide();
    $('.addStereotypeSection').hide();
    $('.myStereotypesSection').hide();
    $('.editStereotypeSection').hide();
    $('.registerSection').hide();
    $('.profileSection').hide();
    $('.TOPStereotypesSetion').hide();
    $('.TOPUsersSection').hide();
};

function showAddStereotypeSection(code) {
    
    $.ajax({
        url: "/Home/AddStereotype/",
        data: { code: code },
        success: function (view) {
            closeModals();
            $('.addStereotypeSection').html(view);
            $('.singleCountrySection').hide();
            $('.addStereotypeSection').fadeIn(300);
        },

    });
};

function closeAddStereotyeSection(code) {
    $.ajax({
        url: "/Home/GetCountryInfo/",
        data: { code: code },
        success: function (view) {
            $('.singleCountrySection').html(view);
            $('.loginSection').hide();
            $('.addStereotypeSection').hide();
            $('.singleCountrySection').fadeIn(300);
        },

    });
}

function closeTOPStereotyesSection() {
    $('.TOPStereotypesSetion').hide();
}

function showMyStereotypesSection() {
    

    $.ajax({
        url: "/Home/MyStereotypes/",
        
        success: function (view) {
            closeModals();
            $('.myStereotypesSection').html(view);
            $('.singleCountrySection').hide();
            $('.loginSection').hide();
            $('.addStereotypeSection').hide();
            $('.myStereotypesSection').fadeIn(300);
        },

    });
}

function closeMyStereotypesSection() {
    $('.myStereotypesSection').hide();
}

function editStereotype(id) {
    $.ajax({
        url: "/Home/EditStereotype/",
        data: {id : id},
        success: function (view) {
            
            $('.editStereotypeSection').html(view);
            $('.myStereotypesSection').hide();
            $('.editStereotypeSection').fadeIn(300);
        },

    });
}

function closeEditStereotypesSection() {
    $('.editStereotypeSection').hide();
    $.ajax({
        url: "/Home/MyStereotypes/",

        success: function (view) {
            $('.myStereotypesSection').html(view);
            $('.singleCountrySection').hide();
            $('.loginSection').hide();
            $('.addStereotypeSection').hide();
            $('.myStereotypesSection').fadeIn(300);
        },

    });
}

function closeRegisterSection() {
    $('.registerSection').hide();
}

function setScore(isAlreadyRated, markID, mark, userID, stereotypeID, authorID) {
    if (userID != "-1") {
        $.ajax({
            url: "/Home/SetScore/",
            data: { isAlreadyRated: isAlreadyRated, markID: markID, mark: mark, userID: userID, stereotypeID: stereotypeID },
            dataType: 'json',
            success: function (response) {


                if (response.responseText == "markWasPositiveAndNowDeleted") {

                    $('#' + stereotypeID + "Rating").text(response.CurrentRating);
                    $('#' + markID + "Positive").removeClass("selected");

                    $('#' + markID + "Positive").attr("onclick", 'setScore("false", -1, 1, ' + response.UserID + ', ' + stereotypeID + ', ' + authorID + ')');
                    $('#' + markID + "Negative").attr("onclick", 'setScore("false", -1, -1, ' + response.UserID + ', ' + stereotypeID + ', ' + authorID + ')');
                    $('#' + markID + "Positive").attr("id", authorID + "and" + stereotypeID + "Positive");
                    $('#' + markID + "Negative").attr("id", authorID + "and" + stereotypeID + "Negative");


                } else if (response.responseText == "markWasNegativeAndNowDeleted") {
                    $('#' + stereotypeID + "Rating").text(response.CurrentRating);
                    $('#' + markID + "Negative").removeClass("selected");

                    $('#' + markID + "Positive").attr("onclick", 'setScore("false", -1, 1, ' + response.UserID + ', ' + stereotypeID + ', ' + authorID + ')');
                    $('#' + markID + "Negative").attr("onclick", 'setScore("false", -1, -1, ' + response.UserID + ', ' + stereotypeID + ', ' + authorID + ')');
                    $('#' + markID + "Positive").attr("id", authorID + "and" + stereotypeID + "Positive");
                    $('#' + markID + "Negative").attr("id", authorID + "and" + stereotypeID + "Negative");

                } else if (response.responseText == "markWasNegativeAndNowPositive") {
                    $('#' + stereotypeID + "Rating").text(response.CurrentRating);
                    $('#' + markID + "Negative").removeClass("selected");
                    $('#' + markID + "Positive").addClass("selected");

                } else if (response.responseText == "markWasPositiveAndNowNegative") {
                    $('#' + stereotypeID + "Rating").text(response.CurrentRating);
                    $('#' + markID + "Positive").removeClass("selected");
                    $('#' + markID + "Negative").addClass("selected");

                } else if (response.responseText == "markWasNothingAndNowPositive") {

                    $('#' + stereotypeID + "Rating").text(response.CurrentRating);

                    $('#' + authorID + "and" + stereotypeID + "Positive").attr("onclick", 'setScore("true", ' + response.NewMarkID + ', 1, ' + response.UserID + ', ' + stereotypeID + ', ' + authorID + ')');
                    $('#' + authorID + "and" + stereotypeID + "Negative").attr("onclick", 'setScore("true", ' + response.NewMarkID + ', -1, ' + response.UserID + ', ' + stereotypeID + ', ' + authorID + ')');
                    $('#' + authorID + "and" + stereotypeID + "Positive").attr("id", response.NewMarkID + "Positive");
                    $('#' + authorID + "and" + stereotypeID + "Negative").attr("id", response.NewMarkID + "Negative");



                    $('#' + response.NewMarkID + "Positive").addClass("selected");

                } else if (response.responseText == "markWasNothingAndNowNegative") {
                    $('#' + stereotypeID + "Rating").text(response.CurrentRating);
                    $('#' + authorID + "and" + stereotypeID + "Positive").attr("onclick", 'setScore("true", ' + response.NewMarkID + ', 1, ' + response.UserID + ', ' + stereotypeID + ')');
                    $('#' + authorID + "and" + stereotypeID + "Negative").attr("onclick", 'setScore("true", ' + response.NewMarkID + ', -1, ' + response.UserID + ', ' + stereotypeID + ')');
                    $('#' + authorID + "and" + stereotypeID + "Positive").attr("id", response.NewMarkID + "Positive");
                    $('#' + authorID + "and" + stereotypeID + "Negative").attr("id", response.NewMarkID + "Negative");


                    $('#' + response.NewMarkID + "Negative").addClass("selected");
                } else {
                    alert("ERROR! UNHANDLED CASE!");
                }
            }, error: function () {
                alert("ERR");
            }

        });
    } else {
        closeModals();
        showLoginContainer();
    }
    
}

function showProfileSection() {
    
    $.ajax({
        url: "/Home/ProfileInfoSection/",
        
        success: function (view) {
            closeModals();
            $('.profileSection').html(view);
            
            $('.profileSection').fadeIn(300);
        },

    });
   
}

function showTOPStereotypes() {
    $.ajax({
        url: "/Home/TOPStereotypes/",

        success: function (view) {
            closeModals();
            $('.TOPStereotypesSetion').html(view);

            $('.TOPStereotypesSetion').fadeIn(300);
        },

    });
}

function showTOPUsers() {
    $.ajax({
        url: "/Home/TOPUsers/",

        success: function (view) {
            closeModals();
            $('.TOPUsersSection').html(view);

            $('.TOPUsersSection').fadeIn(300);
        },

    });
}

function closeTOPUsers() {
    $('.TOPUsersSection').hide();
}

function changeProfile(login, name, email, newPas) {
    $.ajax({
        url: "/Home/ProfileInfo/",
        
        dataType: 'json',
        data: { name: name, email: email, login: login, newPas: newPas },
        success: function (response) {
            if (response.success) {
                window.location.reload();
            } else {
                alert("Wrong input data format!");



            }

        }, error: function () {

            window.location.reload();

        }

    });
}

function closeProfileSection() {
    $('.profileSection').hide();
}