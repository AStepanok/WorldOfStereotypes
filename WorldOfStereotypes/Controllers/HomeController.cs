﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WorldOfStereotypes.Models;

namespace WorldOfStereotypes.Controllers
{
    public class HomeController : Controller
    {

        StereotypesEntities _db = new StereotypesEntities();
        // GET: Home
        public ActionResult Index()
        {
            
            return View();
        }

        
        public ActionResult ProfileInfoSection()
        {
            User u = _db.User.FirstOrDefault(x => x.Login == User.Identity.Name);
            return PartialView(u);
        }

        
        public ActionResult ProfileInfo(string login, string name, string email, string newPas)
        {
            if (ModelState.IsValid)
            {
                User us = _db.User.FirstOrDefault(x => x.Login == login);
                us.Name = name;
                us.Email = email;
                if (!String.IsNullOrWhiteSpace(newPas))
                {
                    us.Password = newPas;
                }
                _db.SaveChanges();
                return Json(new { success = true}, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCountryInfo(string code)
        {
            Country c = _db.Country.FirstOrDefault(x => x.ID == code);

            Directory.CreateDirectory(Server.MapPath("~/Data/Countries/") + c.ID + "/");
            string[] fileNames = Directory.GetFiles(Server.MapPath("~/Data/Countries/") + c.ID + "/");

            ViewBag.Files = fileNames;


            return PartialView(c);
        }

        public ActionResult MyStereotypes()
        {
            User u = _db.User.FirstOrDefault(x => x.Login == User.Identity.Name);
            return PartialView(u);
        }

        [HttpGet]
        public ActionResult EditStereotype(int? id)
        {
            Stereotype s = _db.Stereotype.FirstOrDefault(x => x.ID == id);
            return PartialView(s);
        }

        [HttpPost]
        public ActionResult EditStereotype(HttpPostedFileBase upload, Stereotype s)
        {
           
                User u = _db.User.FirstOrDefault(x => x.Login == User.Identity.Name);
                string id = ViewBag.CountryCode;
                Country c = _db.Country.FirstOrDefault(x => x.ID == s.CountryID);
                Stereotype ster = _db.Stereotype.FirstOrDefault(x=>x.ID == s.ID);
                ster.Name = s.Name;
                ster.Description = s.Description;
                
                
                _db.SaveChanges();
                if (upload != null && !string.IsNullOrEmpty(upload.FileName))
                {
                    FileInfo fi = new FileInfo(upload.FileName);
                    if (fi.Extension.ToLower() == ".png" || fi.Extension.ToLower() == ".jpg" || fi.Extension.ToLower() == ".jpeg")
                    {
                        string fileExtension = Path.GetExtension(upload.FileName);
                        Directory.CreateDirectory(Server.MapPath("~/Data/Countries/") + c.ID + "/");


                        string pathToSave_100 = Server.MapPath("~/Data/Countries/") + c.ID + "/" + ster.ID + fileExtension;
                        upload.SaveAs(pathToSave_100);

                        pathToSave_100 = "/Data/Countries/" + c.ID + "/" + ster.ID + fileExtension;

                        ster.Image = pathToSave_100;
                        _db.SaveChanges();
                    }


                }

                return RedirectToAction("Index");
            

            return PartialView(s);
        }

        public ActionResult TOPStereotypes()
        {
            return PartialView(_db.Stereotype.ToList());
        }

        public ActionResult TOPUsers()
        {
            return PartialView(_db.User.ToList().OrderBy(x => x.Rating).Reverse());
        }

        public ActionResult DeleteStereotype(int? id)
        {
            Stereotype s = _db.Stereotype.FirstOrDefault(x => x.ID == id);
            
            User u = _db.User.FirstOrDefault(x => x.ID == s.UserID);
            u.Rating -= s.Rating;
            u.Stereotype.Remove(s);
            foreach(Mark m in s.Mark)
            {
                User us = _db.User.FirstOrDefault(x => x.ID == m.UserID);
                us.Mark.Remove(m);
            }
            
            s.User = null;
            _db.Mark.RemoveRange(s.Mark);
            s.Mark.Clear();
            _db.Stereotype.Remove(s);
            _db.SaveChanges();

            System.IO.File.Delete(Server.MapPath(s.Image));

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult AddStereotype(string code)
        {
            Country c = _db.Country.FirstOrDefault(x => x.ID == code);
            ViewBag.CountryCode = c.ID;
            return PartialView();
        }


        [HttpPost]
        public ActionResult AddStereotype(HttpPostedFileBase upload, Stereotype s)
        {
            if (ModelState.IsValid)
            {

                User u = _db.User.FirstOrDefault(x => x.Login == User.Identity.Name);
                string id = ViewBag.CountryCode;
                Country c = _db.Country.FirstOrDefault(x => x.ID == s.CountryID);
                Stereotype ster = new Stereotype();
                ster.Name = s.Name;
                ster.Description = s.Description;
                ster.Country = c;
                ster.CountryID = c.ID;
                ster.Rating = 0;
                ster.Image = "";
                ster.UserID = u.ID;
                ster.User = u;
                _db.Stereotype.Add(ster);
                _db.SaveChanges();
                if (!string.IsNullOrEmpty(upload.FileName))
                {
                    FileInfo fi = new FileInfo(upload.FileName);
                    if (fi.Extension.ToLower() == ".png" || fi.Extension.ToLower() == ".jpg" || fi.Extension.ToLower() == ".jpeg")
                    {
                        string fileExtension = Path.GetExtension(upload.FileName);
                        Directory.CreateDirectory(Server.MapPath("~/Data/Countries/") + c.ID + "/");


                        string pathToSave_100 = Server.MapPath("~/Data/Countries/") + c.ID + "/" + ster.ID + fileExtension;
                        upload.SaveAs(pathToSave_100);

                        pathToSave_100 = "/Data/Countries/" + c.ID + "/" + ster.ID + fileExtension;

                        ster.Image = pathToSave_100;
                        _db.SaveChanges();
                    }


                }

                return RedirectToAction("Index");
            }

            return PartialView(s);
        }

        public ActionResult SetScore(string isAlreadyRated, string markID, string mark, string userID, string stereotypeID)
        {
            Mark m = _db.Mark.FirstOrDefault(x => x.ID.ToString() == markID);
            User cU = _db.User.FirstOrDefault(x => x.ID.ToString() == userID);
            if (cU != null)
            {
                if (isAlreadyRated == "true")
                {
                    


                    Stereotype s = _db.Stereotype.FirstOrDefault(x => x.ID == m.StereotypeID);
                    User u = _db.User.FirstOrDefault(x => x.ID == s.UserID);
                    if (mark == "1")
                    {
                        if (m.Mark1 == 1)
                        {

                            u.Rating -= 1;
                            s.Rating -= 1;
                            s.Mark.Remove(m);
                            cU.Mark.Remove(m);
                            _db.Mark.Remove(m);
                            _db.SaveChanges();
                            return Json(new { success = true, responseText = "markWasPositiveAndNowDeleted", CurrentRating = s.Rating, NewMarkID = m.ID, UserID = cU.ID }, JsonRequestBehavior.AllowGet); ;
                        }
                        else
                        {
                            m.Mark1 = 1;
                            s.Rating += 2;
                            u.Rating += 2;
                            _db.SaveChanges();
                            return Json(new { success = true, responseText = "markWasNegativeAndNowPositive", CurrentRating = s.Rating, NewMarkID = m.ID, UserID = cU.ID }, JsonRequestBehavior.AllowGet); ;
                        }
                    }
                    else
                    {
                        if (m.Mark1 == 1)
                        {

                            m.Mark1 = -1;
                            s.Rating -= 2;
                            u.Rating -= 2;
                            _db.SaveChanges();
                            return Json(new { success = true, responseText = "markWasPositiveAndNowNegative", CurrentRating = s.Rating, NewMarkID = m.ID, UserID = cU.ID }, JsonRequestBehavior.AllowGet); ;
                        }
                        else
                        {
                            u.Rating += 1;
                            s.Rating += 1;
                            s.Mark.Remove(m);
                            cU.Mark.Remove(m);
                            _db.Mark.Remove(m);
                            _db.SaveChanges();
                            return Json(new { success = true, responseText = "markWasNegativeAndNowDeleted", CurrentRating = s.Rating, NewMarkID = m.ID, UserID = cU.ID }, JsonRequestBehavior.AllowGet); ;
                        }
                    }
                }
                else
                {
                    
                    Stereotype s = _db.Stereotype.FirstOrDefault(x => x.ID.ToString() == stereotypeID);
                    User u = _db.User.FirstOrDefault(x => x.ID == s.UserID);
                    if (mark == "1")
                    {
                        m = new Mark();
                        m.Mark1 = 1;
                        m.StereotypeID = s.ID;
                        m.Stereotype = s;
                        m.UserID = cU.ID;
                        m.User = cU;
                        u.Rating += 1;
                        s.Rating += 1;
                        _db.Mark.Add(m);
                        _db.SaveChanges();
                        return Json(new { success = true, responseText = "markWasNothingAndNowPositive", CurrentRating = s.Rating, NewMarkID = m.ID, UserID = cU.ID }, JsonRequestBehavior.AllowGet); ;
                    }
                    else
                    {
                        m = new Mark();
                        m.Mark1 = -1;
                        m.StereotypeID = s.ID;
                        m.Stereotype = s;
                        m.UserID = cU.ID;
                        m.User = cU;
                        u.Rating -= 1;
                        s.Rating -= 1;
                        _db.Mark.Add(m);
                        _db.SaveChanges();
                        return Json(new { success = true, responseText = "markWasNothingAndNowNegative", CurrentRating = s.Rating, NewMarkID = m.ID, UserID = cU.ID }, JsonRequestBehavior.AllowGet); ;
                    }
                }
            } else
            {
                return Json(new { success = false, responseText = "noUser"}, JsonRequestBehavior.AllowGet); ;
            }
        }

        public ActionResult Login(string log, string pas)
        {
            if (ModelState.IsValid)
            {
                User user = _db.User.FirstOrDefault(x => x.Login.ToLower() == log.ToLower() && x.Password == pas);
                if (user != null)
                {
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                            10,
                            user.Login,
                            DateTime.Now,
                            DateTime.Now.AddMinutes(40),
                            true,
                            "user"
                            );


                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                   
                    cookie.Expires = authTicket.Expiration;
                    Response.Cookies.Add(cookie);

                    return RedirectToAction("Index");
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult Register(string nam, string email, string log, string pas)
        {
            if (ModelState.IsValid && !String.IsNullOrWhiteSpace(nam) && !String.IsNullOrWhiteSpace(email) && !String.IsNullOrWhiteSpace(log) && !String.IsNullOrWhiteSpace(pas))
            {
                User u = _db.User.FirstOrDefault(x => x.Login.ToLower() == log.ToLower());
                if (u == null)
                {
                    User us = new User();
                    us.Name = nam;
                    us.Email = email;
                    us.Login = log;
                    us.Password = pas;
                    _db.User.Add(us);
                    _db.SaveChanges();

                    return RedirectToAction("Login", new { log = log, pas = pas });
                }
                return Json(new { success = false, responseText = "User with this login is already exists!" }, JsonRequestBehavior.AllowGet); ;
            }
            return Json(new { success = false, responseText = "Incorrect format of input data!" }, JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}